export const ADD_TASK = 'ADD_TASK';
export const CLEAR_ALL_TASKS = 'CLEAR_ALL_TASKS';
export const TRANSFER_TASK = 'TRANSFER_TASK';
export const DELETE_TASK = 'DELETE_TASK';
