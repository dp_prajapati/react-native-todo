import * as action from './actionTypes';
export function addTask(task) {
  return {
    type: action.ADD_TASK,
    payload: {
      task,
    },
  };
}

export function clearAllTasks() {
  return {
    type: action.CLEAR_ALL_TASKS,
  };
}

export function deleteTask(key, index) {
  return {
    type: action.DELETE_TASK,
    payload: {
      key,
      index,
    },
  };
}

export function transferTask(fromKey, toKey, index) {
  return {
    type: action.TRANSFER_TASK,
    payload: {
      toKey,
      fromKey,
      index,
    },
  };
}
