import {Alert} from 'react-native';
import * as actions from './actionTypes';
var ttasks = [];
var ctasks = [];

const initialTasks = {
  todoTasks: ttasks,
  completedTasks: ctasks,
  id: ttasks.length + ctasks.length,
};

const rootReducer = (state = initialTasks, action) => {
  switch (action.type) {
    case actions.ADD_TASK: {
      if (action.payload.task.length <= 3) {
        Alert.alert('OOPS!!', 'todo-task must be 4 chars long', [
          {
            text: 'Okay',
            onPress: () => {},
          },
        ]);
        return state;
      } else {
        const tasks = state.todoTasks.slice();
        tasks.push({task: action.payload.task, id: state.id});
        return {
          ...state,
          todoTasks: tasks,
          id: state.id + 1,
        };
      }
    }

    case actions.DELETE_TASK: {
      const {key, index} = action.payload;
      const tasks = state[key].slice();
      tasks.splice(index, 1);
      return {
        ...state,
        [key]: tasks,
      };
    }

    case actions.TRANSFER_TASK: {
      const {toKey, fromKey, index} = action.payload;
      const fromTasks = state[fromKey].slice();
      const toTasks = state[toKey].slice();
      toTasks.push(fromTasks[index]);
      fromTasks.splice(index, 1);
      return {
        ...state,
        [toKey]: toTasks,
        [fromKey]: fromTasks,
      };
    }

    case actions.CLEAR_ALL_TASKS: {
      return {todoTasks: [], completedTasks: []};
    }

    default:
      return state;
  }
};

export default rootReducer;
