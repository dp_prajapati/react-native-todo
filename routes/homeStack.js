import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';
import React from 'react';
import Header from '../shared/Header';
import Home from '../components/Home';
import Todo from '../Todo';
const screens = {
  Home: {
    screen: Home,
  },
  Todo: {
    screen: Todo,
    navigationOptions: {
      title: 'Todo-App',
    },
  },
};

// home stack navigator screens
const HomeStack = createStackNavigator(screens);

export default createAppContainer(HomeStack);
