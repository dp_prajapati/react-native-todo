import React from 'react';
import {ScrollView, StyleSheet, Text, View, Button, Alert} from 'react-native';
import Header from './components/Header';
import TaskList from './components/TaskList';
import Clear from './components/Clear';
import Card from './shared/Card';
const Todo = () => {
  return (
    <>
      <Header />
      <Card>
        <View style={styles.title}>
          <Text style={styles.title}>todo-tasks :</Text>
        </View>
        <ScrollView>
          <TaskList fromKey="todoTasks" toKey="completedTasks" />
        </ScrollView>
      </Card>

      <Card>
        <View style={styles.title}>
          <Text style={styles.title}>completed-tasks :</Text>
        </View>
        <ScrollView>
          <TaskList toKey="todoTasks" fromKey="completedTasks" />
        </ScrollView>
      </Card>
      <Clear />
    </>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 20,
    fontWeight: '400',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Todo;
