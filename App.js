import React from 'react';
import Todo from './Todo';
import Navigator from './routes/homeStack';
import {View} from 'react-native';
import {Provider} from 'react-redux';
import store from './redux/store';
const App = () => {
  return (
    <Provider store={store}>
      <Navigator />
    </Provider>
  );
};

export default App;
