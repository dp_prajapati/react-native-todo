import React from 'react';

import {View, StyleSheet, Button} from 'react-native';

function Home({navigation}) {
  return (
    <View style={styles.container}>
      <Button
        title="Start"
        onPress={() => {
          navigation.navigate('Todo');
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#b127db',
  },
});

export default Home;
