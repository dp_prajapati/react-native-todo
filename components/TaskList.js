import React from 'react';
import {View} from 'react-native';
import Task from './Task';
import {connect} from 'react-redux';
function TaskList(props) {
  const {fromKey, toKey, tasks} = props;
  return (
    <View>
      {tasks.map((task, index) => {
        return (
          <Task key={task.id} index={index} fromKey={fromKey} toKey={toKey} />
        );
      })}
    </View>
  );
}

const mapStateToProps = (state, ownProps) => {
  return {
    tasks: state[ownProps.fromKey],
  };
};

export default connect(mapStateToProps)(TaskList);
