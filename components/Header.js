import React from 'react';
import {useState} from 'react';
import {View, Text, StyleSheet, TextInput, Button} from 'react-native';
import {connect} from 'react-redux';
import {addTask} from '../redux/actions';
function Header(props) {
  const [input, setInput] = useState('');
  const handleInput = task => {
    setInput(task);
  };
  return (
    <>
      <View style={styles.header}>
        <Text style={styles.titleElement}>Todo-App</Text>
        <View style={styles.container}>
          <TextInput
            style={styles.inputElement}
            value={input}
            onChangeText={handleInput}
          />
          <Button
            title="ADD"
            color={'black'}
            onPress={() => {
              props.onClick(addTask(input));
              setInput('');
            }}
          />
        </View>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  titleElement: {
    fontSize: 25,
    color: 'black',
    paddingBottom: 15,
    fontWeight: '500',
  },
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    color: 'black',
  },
  inputElement: {
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    width: '80%',
  },
  header: {
    width: '100%',
    height: 90,
    backgroundColor: '#f7287b',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const mapDispatchToProps = dispatch => {
  return {
    onClick: dispatch,
  };
};

export default connect(null, mapDispatchToProps)(Header);
