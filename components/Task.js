import React from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';
import {connect} from 'react-redux';
import {transferTask, deleteTask} from '../redux/actions';
function Task(props) {
  const {onTransfer, task, onDelete} = props;

  return (
    <View style={styles.task}>
      <View>
        <Text>{task}</Text>
      </View>
      <View style={styles.button}>
        <Button title="Toggle" onPress={onTransfer} />
        <Button title="Delete" onPress={onDelete} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  task: {
    height: 40,
    backgroundColor: '#d4bc4d',
    borderColor: 'black',
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: 5,
    marginVertical: 5,
  },
  button: {
    flexDirection: 'row',
  },
});

const mapStateToProps = (state, ownProps) => {
  return {
    task: state[ownProps.fromKey][ownProps.index].task,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  var {fromKey, toKey, index} = ownProps;
  return {
    onDelete: () => {
      dispatch(deleteTask(fromKey, index));
    },
    onTransfer: () => {
      dispatch(transferTask(fromKey, toKey, index));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Task);
