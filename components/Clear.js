import React from 'react';
import {connect} from 'react-redux';
import {clearAllTasks} from '../redux/actions';
import {Button} from 'react-native';

function Clear(props) {
  return <Button title="Clear All Tasks" onPress={props.onClick} />;
}

const mapDispatchToProps = dispatch => {
  return {
    onClick: () => {
      dispatch(clearAllTasks());
    },
  };
};

export default connect(null, mapDispatchToProps)(Clear);
