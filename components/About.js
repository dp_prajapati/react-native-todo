import React from 'react';

import {View, StyleSheet, Text} from 'react-native';

function About() {
  return (
    <View style={styles.container}>
      <Text>This Page is Empty!!</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#b127db',
  },
});

export default About;
