import React from 'react';
import {View, StyleSheet} from 'react-native';
function Card(props) {
  return <View style={styles.card}>{props.children}</View>;
}

const styles = StyleSheet.create({
  card: {
    shadowColor: 'black',
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 6,
    shadowOpacity: 0.3,
    backgroundColor: 'white',
    height: '35%',
    marginVertical: 10,
    borderRadius: 10,
    marginHorizontal: 10,
  },
});

export default Card;
